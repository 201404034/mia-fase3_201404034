"""Fase3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('Portada.urls', namespace='index'), name='index'),
    url(r'^Admin/', include('Administrador.urls', namespace='Admin'), name='Admin'),
    url(r'^Usuarios/', include('Clientes.urls', namespace='Usuarios'), name='Usuarios'),
    url(r'^Categorias/', include('Categorias.urls', namespace='Categorias'), name='Categorias'),
    url(r'^Productos/', include('Productos.urls', namespace='Productos'), name='Productos'),
    url(r'^Compras/', include('Compras.urls', namespace='Compras'), name='Compras'),
    url(r'^Catalogo/', include('VistaCaquera.urls', namespace='Ver'), name='Ver'),
    url(r'^Colores/', include('Colores.urls', namespace='Colores'), name='Colores'),
    url(r'^Comentarios/', include('Comentarios.urls', namespace='Comentarios'), name='Comentarios'),
    url(r'^Reportes/',include('Reportes.urls', namespace='Reportes'), name='Reportes'),
]
