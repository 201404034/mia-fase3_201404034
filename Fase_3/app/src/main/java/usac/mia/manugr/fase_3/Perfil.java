package usac.mia.manugr.fase_3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Statement;

public class Perfil extends Activity {

    TextView nombre, genero, fecha, correo;
    EditText apellido, direccion, telefono;
    ImageView foto;
    Button carrito, catalogo, logout, actualizar;
    Statement st;
    Usuario usu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        nombre = (TextView) findViewById(R.id.et_Nombre_Perfil);
        apellido = (EditText) findViewById(R.id.et_Apellido_Perfil);
        direccion = (EditText) findViewById(R.id.et_Direccion_Perfil);
        telefono = (EditText) findViewById(R.id.et_Telefono_Perfil);
        genero = (TextView) findViewById(R.id.et_Genero_Perfil);
        fecha = (TextView) findViewById(R.id.et_Fecha_Perfil);
        correo = (TextView) findViewById(R.id.txt_Correo_Perfil);

        foto = (ImageView) findViewById(R.id.Foto_Perfil);

        Intent i = getIntent();
        usu = (Usuario) i.getSerializableExtra("Usuario");

        nombre.setText(usu.nombre);
        apellido.setText(usu.apellido);
        direccion.setText(usu.direccion);
        telefono.setText(usu.telefono);
        genero.setText(usu.genero);
        fecha.setText(usu.nacimiento);
        correo.setText(usu.correo);

        try {
            URL url = new URL(usu.foto);
            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            foto.setImageBitmap(bmp);
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        actualizar = (Button) findViewById(R.id.btn_Actualizar_Perfil);
        actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ConnectOra conn = new ConnectOra("10.0.2.2", "XE", "Fase_3", "1234");
                    st = conn.getConexion().createStatement();
                    String papaQuery = "UPDATE Usuario " +
                            "SET APELLIDO = '"+apellido.getText().toString()+"'," +
                            " DIRECCION = '"+direccion.getText().toString()+"'," +
                            " TELEFONO = '"+telefono.getText().toString()+"' " +
                            " WHERE CORREO = "+correo.getText().toString();
                    ResultSet resultado = st.executeQuery(papaQuery);

                    if(resultado.rowUpdated()){
                        Toast.makeText(getApplicationContext(), "Actualizado exitosamente!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Algo salio mal", Toast.LENGTH_SHORT).show();
                    }
                    resultado.close();
                    st.close();
                    Intent i = new Intent(getApplicationContext(), Perfil.class);
                    startActivity(i);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });


        carrito = (Button) findViewById(R.id.btn_Carrito_Perfil);
        /*carrito.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), Carrito.class);
                startActivity(i);
            }
        });*/
        catalogo = (Button) findViewById(R.id.btn_Catalogo_Perfil);
        catalogo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), Catalogo.class);
                startActivity(i);
            }
        });
        logout = (Button) findViewById(R.id.btn_Logout_Perfil);
        logout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), Portada.class);
                startActivity(i);
            }
        });
    }
}
