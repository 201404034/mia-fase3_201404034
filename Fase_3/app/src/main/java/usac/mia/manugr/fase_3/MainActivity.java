package usac.mia.manugr.fase_3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.ResultSet;
import java.sql.Statement;

public class MainActivity extends Activity {

    Statement st;
    EditText usuario, password;
    Usuario usu;
    Button registro, login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usuario = (EditText) findViewById(R.id.et_Usuario_Login);
        password = (EditText) findViewById(R.id.et_Password_Login);
        usu = new Usuario();
        login = (Button) findViewById(R.id.btn_Login);
        registro = (Button) findViewById(R.id.btn_Registro);
        registro.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), Registro.class);
                startActivity(i);
            }
        });
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    ConnectOra conn = new ConnectOra("10.0.2.2", "XE", "Fase_3", "1234");
                    st = conn.getConexion().createStatement();
                    ResultSet resultado = st.executeQuery("SELECT * From USUARIO WHERE CORREO = '" + usuario.getText().toString() + "'");

                    if (resultado.next()) {

                        usu.nombre = resultado.getString("NOMBRE");
                        usu.apellido = resultado.getString("APELLIDO");
                        usu.correo = resultado.getString("CORREO");
                        usu.direccion = resultado.getString("DIRECCION");
                        usu.telefono = resultado.getString("TELEFONO");
                        usu.genero = resultado.getString("GENERO");
                        usu.nacimiento = resultado.getString("FECHA_DE_NACIMIENTO");
                        usu.foto = "http://10.0.2.2:8000/media/";
                        usu.foto += resultado.getString("FOTO");
                        usu.rol = resultado.getString("ROL_ID");
                    } else {
                        Toast.makeText(getApplicationContext(), "No paso wey", Toast.LENGTH_SHORT).show();
                    }
                    if (usu.nombre.equals("")) {
                        Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent i;
                        if(usu.rol.equals("1")){
                            i = new Intent(getApplicationContext(), Perfil.class);
                        }else if(usu.rol.equals("2")){
                            i = new Intent(getApplicationContext(), HelpDesk.class);
                        }else if(usu.rol.equals("3")){
                            i = new Intent(getApplicationContext(), Administrador.class);
                        }else{
                            i = new Intent(getApplicationContext(), Portada.class);
                        }

                        i.putExtra("Usuario", usu);
                        startActivity(i);
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
