from django.contrib import admin
from .models import Producto, Usuario, Factura, Carrito, Compra

# Register your models here.
@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
    pass

@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    pass

@admin.register(Factura)
class FacturaAdmin(admin.ModelAdmin):
    pass

@admin.register(Carrito)
class CarritoAdmin(admin.ModelAdmin):
    pass

@admin.register(Compra)
class CompraAdmin(admin.ModelAdmin):
    pass