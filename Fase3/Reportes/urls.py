from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import(
	Promedio_Helpdesk,
	Machos_Helpdesk,
	Administradoras,
	Fichudos,
	Promedio_Productos,
	Top_Productos,
	Top_Vendedores,
	Productos_Lista,
	Productos_Disponibles,
	Promedio_Helpdesk_pdf,
	Machos_Helpdesk_pdf,
	Administradoras_pdf,
	Fichudos_pdf,
	Promedio_Productos_pdf,
	Top_Productos_pdf,
	Top_Vendedores_pdf,
	Productos_Lista_pdf,
	Productos_Disponibles_pdf
)

urlpatterns = [
	url(r'^HelpDesk_Promedios/$', Promedio_Helpdesk, name='helpdeskProm'),
	url(r'^HelpDesk_Machos/$', Machos_Helpdesk, name='helpdeskMacho'),
	url(r'^Administradoras/$', Administradoras, name='administradoras'),
	url(r'^Fichudos/$', Fichudos, name='fichudos'),
	url(r'^Productos_Promedios/$', Promedio_Productos, name='productosProm'),
	url(r'^Top_Productos/$', Top_Productos, name='topProductos'),
	url(r'^Top_Vendedores/$', Top_Vendedores, name='topVendedores'),
	url(r'^Productos_Lista/$', Productos_Lista, name='productosListado'),
	url(r'^Productos_Disponibles/$', Productos_Disponibles, name='productosDisponibles'),
	#PDF's
	url(r'^HelpDesk_Promedios_pdf/$', Promedio_Helpdesk_pdf, name='helpdeskProm_pdf'),
	url(r'^HelpDesk_Machos_pdf/(?P<year>\d+)$', Machos_Helpdesk_pdf, name='helpdeskMacho_pdf'),
	url(r'^Administradoras_pdf/(?P<year>\d+)$', Administradoras_pdf, name='administradoras_pdf'),
	url(r'^Fichudos_pdf/$', Fichudos_pdf, name='fichudos_pdf'),
	url(r'^Productos_Promedios_pdf/$', Promedio_Productos_pdf, name='productosProm_pdf'),
	url(r'^Top_Productos_pdf/$', Top_Productos_pdf, name='topProductos_pdf'),
	url(r'^Top_Vendedores_pdf/$', Top_Vendedores_pdf, name='topVendedores_pdf'),
	url(r'^Productos_Lista_pdf/$', Productos_Lista_pdf, name='productosListado_pdf'),
	url(r'^Productos_Disponibles_pdf/(?P<cantidad>\d+)$', Productos_Disponibles_pdf, name='productosDisponibles_pdf'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)