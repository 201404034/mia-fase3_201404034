from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)
from django.shortcuts import render
from .models import Categoria
from .forms import Categoria_Form

# Create your views here.
############################
#        Categorias        #
############################
class CategoriaList(ListView):
    model = Categoria
    paginate_by = 10

class CategoriaDetail(DetailView):
    model = Categoria

class CategoriaCreation(CreateView):
    model = Categoria
    success_url = reverse_lazy('Categorias:list')
    form_class = Categoria_Form

class CategoriaUpdate(UpdateView):
    model = Categoria
    success_url = reverse_lazy('Categorias:list')
    form_class = Categoria_Form

class CategoriaDelete(DeleteView):
    model = Categoria
    success_url = reverse_lazy('Categorias:list')