from django.conf.urls import url
from .views import (
	CategoriaList,
	CategoriaDetail,
	CategoriaCreation,
	CategoriaUpdate,
	CategoriaDelete
)

urlpatterns = [
###############
#  Categoria  #
###############
	url(r'^$', CategoriaList.as_view(), name='list'),
    url(r'^(?P<pk>\d+)$', CategoriaDetail.as_view(), name='detail'),
    url(r'^nuevo$', CategoriaCreation.as_view(), name='new'),
    url(r'^editar/(?P<pk>\d+)$', CategoriaUpdate.as_view(), name='edit'),
    url(r'^borrar/(?P<pk>\d+)$', CategoriaDelete.as_view(), name='delete'),
]