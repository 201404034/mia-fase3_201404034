from django.shortcuts import render
from django.views.generic import TemplateView, FormView
from .forms import Usuario_Form, Perfil_Form
from django.core.urlresolvers import reverse_lazy
from .models import Usuario, Rol, Genero, Carrito
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.core.mail import EmailMessage, send_mail
from django.http import HttpResponseRedirect

# Create your views here.
def carrito(request):
    real = Usuario.objects.get(magia = request.user)
    car = Carrito()
    car.usuario = real
    car.save()
    return HttpResponseRedirect(reverse_lazy('index:home'))

class Registrarse(FormView):
    template_name = 'Clientes/Registro.html'
    form_class = Usuario_Form
    success_url = reverse_lazy('index:home')

    def form_valid(self, form):
        try:
            user = form.save(commit=False)
            user.EMAIL = form.cleaned_data['correo']
            user.FIRST_NAME = form.cleaned_data['nombre']
            user.LAST_NAME = form.cleaned_data['apellido']
            real = Usuario()
            real.password = form.cleaned_data['password1']

            mayuscula = False
            minuscula = False
            numero = False
            simbolo = False
            if len(real.password) < 8:
                raise Exception('El password es menor a 8 caracteres')
            for car in real.password:
                if car.isupper() == True:
                    mayuscula = True
                if car.islower() == True:
                    minuscula = True
                if car.isdigit() == True:
                    numero = True
                if car == '!':
                    simbolo = True
                if car == '@':
                    simbolo = True
                if car == '#':
                    simbolo = True
                if car == '$':
                    simbolo = True
                if car == '%':
                    simbolo = True
                if car == '^':
                    simbolo = True
                if car == '&':
                    simbolo = True
                if car == '*':
                    simbolo = True
                if car == '(':
                    simbolo = True
                if car == ')':
                    simbolo = True
                if car == '-':
                    simbolo = True
                if car == '=':
                    simbolo = True
                if car == '+':
                    simbolo = True
                if car == '?':
                    simbolo = True
            if mayuscula and minuscula and simbolo and numero:
                user.save()
                real.magia = user
                real.nombre = form.cleaned_data['nombre']
                real.apellido = form.cleaned_data['apellido']
                real.correo = form.cleaned_data['correo']
                real.telefono = form.cleaned_data['telefono']
                real.foto = form.cleaned_data['foto']
                quiero = form.cleaned_data['genero']
                sexo = Genero.objects.get(genero = quiero)
                real.genero = sexo
                real.fecha_de_nacimineto = form.data['fecha_de_nacimineto_year'] + '-' + form.data['fecha_de_nacimineto_month'] + '-' + form.data['fecha_de_nacimineto_day']
                real.direccion = form.cleaned_data['direccion']
                real.correo = form.cleaned_data['correo']
                tipo = Rol.objects.get(nombre='Cliente')
                real.rol = tipo
                real.ganancia_obtenida = 0
                real.clase_cliente = form.cleaned_data['clase_cliente']
                if real.clase_cliente is 'A':
                    real.credito_disponible = 20000
                elif real.clase_cliente is 'B':
                    real.credito_disponible = 15000
                elif real.clase_cliente is 'C':
                    real.credito_disponible = 10000
                elif real.clase_cliente is 'D':
                    real.credito_disponible = 5000
                elif real.clase_cliente is 'E':
                    real.credito_disponible = 1000
                else:
                    real.credito_disponible = 1000
                    real.clase_cliente = 'A'
                real.save()
                asunto = '[Publishers and Sells]'
                mensaje = 'Gracias por registrarte con nosotros!\n\tTu usuario es:  ' + form.cleaned_data['username'] + '\n\tTu password es: ' + form.cleaned_data['password1']
                enviador = 'manugr21@gmail.com'
                receptor = [real.correo]
                send_mail(asunto, mensaje, enviador, receptor, fail_silently=True)
            else:
                raise Exception('El password debe tener un numero una mayuscula, una minuscula y un sumbolo y mas de 8 caracteres')
        except Exception as e:
            raise e
        return super(Registrarse, self).form_valid(form)


class Perfil(DetailView):
    model = Usuario
    
class Perfil_Update(UpdateView):
    model = Usuario
    form_class = Perfil_Form
    success_url = reverse_lazy('Usuarios:Perfil')