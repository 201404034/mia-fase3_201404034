from django.conf.urls import url
from .views import (
	CompraList,
	CompraDetail,
	CompraCreation,
	CompraCreationM,
	CompraUpdate,
	CompraDelete
)

urlpatterns = [
###############
#   Compra    #
###############
	url(r'^$', CompraList.as_view(), name='list'),
    url(r'^(?P<pk>\d+)$', CompraDetail.as_view(), name='detail'),
    url(r'^nuevo/(?P<cantidad>\d+)$', CompraCreation.as_view(), name='new'),
    url(r'^editar/(?P<pk>\d+)$', CompraUpdate.as_view(), name='edit'),
    url(r'^borrar/(?P<pk>\d+)$', CompraDelete.as_view(), name='delete'),
    url(r'^nuevo$', CompraCreationM.as_view(), name='newM'),
]