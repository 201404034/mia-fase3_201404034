from django.apps import AppConfig

class UsuariosConfig(AppConfig):
    name = 'Usuarios'

class CategoriasConfig(AppConfig):
    name = 'Categorias'