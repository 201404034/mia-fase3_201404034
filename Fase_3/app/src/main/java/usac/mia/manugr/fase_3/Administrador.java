package usac.mia.manugr.fase_3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;
import java.util.List;

public class Administrador extends Activity {

    TextView nombre, apellido, direccion, telefono, genero, fecha, correo;
    Button crud, portada, logout;

    Usuario usu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_administrador);

        nombre = (TextView) findViewById(R.id.et_Nombre_Administrador);
        apellido = (TextView) findViewById(R.id.et_Apellido_Administrador);
        direccion = (TextView) findViewById(R.id.et_Direccion_Administrador);
        telefono = (TextView) findViewById(R.id.et_Telefono_Administrador);
        genero = (TextView) findViewById(R.id.et_Genero_Administrador);
        fecha = (TextView) findViewById(R.id.et_Fecha_Administrador);
        correo = (TextView) findViewById(R.id.txt_Correo_Administrador);

        Intent i = getIntent();
        usu = (Usuario) i.getSerializableExtra("Usuario");

        nombre.setText(usu.nombre);
        apellido.setText(usu.apellido);
        direccion.setText(usu.direccion);
        telefono.setText(usu.telefono);
        genero.setText(usu.genero);
        fecha.setText(usu.nacimiento);
        correo.setText(usu.correo);

        crud = (Button) findViewById(R.id.btn_Crud_Administrador);
        crud.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), ListaDeUsuarios.class);
                startActivity(i);
            }
        });
        portada = (Button) findViewById(R.id.btn_Update_Administrador);
        portada.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), UpdatePortada.class);
                startActivity(i);
            }
        });
        logout = (Button) findViewById(R.id.btn_Logout_Administrador);
        logout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), Portada.class);
                startActivity(i);
            }
        });
    }
}
