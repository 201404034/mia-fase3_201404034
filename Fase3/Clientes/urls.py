from django.conf.urls import patterns, include, url
from .views import Registrarse, Perfil, Perfil_Update, carrito
from django.contrib.auth.views import login, logout_then_login

urlpatterns = patterns('', 
    url(r'^LogIn/$', login,
        {'template_name': 'Clientes/LogIn.html'}, name='login'),

    url(r'^LogOut/$', logout_then_login,
        name='logout'),

    url(r'^Registro/$', Registrarse.as_view(),
        name='Registrarse'),

    url(r'^Perfil/(?P<pk>\d+)$', Perfil.as_view(),
        name='Perfil'),

    url(r'^editar/(?P<pk>\d+)$', Perfil_Update.as_view(), name='Cambiar'),

    url(r'^nc/$', carrito,
        name='newCarrito'),
)