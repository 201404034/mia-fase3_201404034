from django import forms 
from django.forms import extras, ModelForm
from django.contrib.auth.forms import UserCreationForm
from .models import Genero, Usuario


cliente_clase =(
    ('A', 'A'),
    ('B', 'B'),
    ('C', 'C'),
    ('D', 'D'),
    ('E', 'E'),
)

class Usuario_Form(UserCreationForm):
    nombre = forms.CharField()
    apellido = forms.CharField()
    correo = forms.EmailField()
    telefono = forms.CharField()
    foto = forms.ImageField()
    genero = forms.ModelChoiceField(queryset= Genero.objects.all())
    fecha_de_nacimineto = forms.DateField(widget = extras.SelectDateWidget(years=range(1970, 2017)))
    direccion = forms.CharField()
    clase_cliente = forms.ChoiceField(
        required=True,
        choices=cliente_clase,
    )

class Perfil_Form(ModelForm):
    class Meta:
        model = Usuario
        fields=[
        'nombre',
        'apellido',
        'correo',
        'telefono',
        'foto',
        'genero',
        'fecha_de_nacimineto',
        'direccion'
        ]