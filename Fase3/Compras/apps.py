from django.apps import AppConfig

class ProductosConfig(AppConfig):
    name = 'Productos'

class UsuariosConfig(AppConfig):
    name = 'Usuarios'
    
class FacturasConfig(AppConfig):
    name = 'Facturas'
    
class ComprasConfig(AppConfig):
    name = 'Compras'
    
class CarritosConfig(AppConfig):
    name = 'Carritos'