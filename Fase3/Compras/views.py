from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (CreateView, UpdateView, DeleteView)
from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from .models import Producto, Usuario, Carrito, Compra, Categoria
from .forms import Compra_Form

# Create your views here.
############################
#         Compras        #
############################
class CompraList(ListView):   
    model = Compra
    paginate_by = 10
    #usu = Usuario.objects.get(magia = User or None)
    #queryset = super(CompraDetail, self).get_queryset()

class CompraDetail(DetailView):
    model = Compra

class CompraCreation(CreateView):
    model = Compra
    success_url = reverse_lazy('Productos:list')
    form_class = Compra_Form

    def form_valid(self, form, **kwargs):
        try:
            compra = form.save(commit=False)
            usu = Usuario.objects.get(magia = self.request.user)
            car = Carrito.objects.get(usuario = usu)
            compra.carrito = car
            compra.producto = Producto.objects.get(id = self.kwargs['cantidad'])
            compra.categoria = Categoria.objects.get(id = compra.producto.categoria.id)
            compra.save()
        except Exception as e:
            raise e
        return super(CompraCreation, self).form_valid(form)

class CompraCreationM(CreateView):
    model = Compra
    success_url = reverse_lazy('Productos:list')
    fields = ['cantidad', 'producto']

    def form_valid(self, form, **kwargs):
        try:
            compra = form.save(commit=False)
            usu = Usuario.objects.get(magia = self.request.user)
            car = Carrito.objects.get(usuario = usu)
            compra.carrito = car
            compra.categoria = Categoria.objects.get(id = compra.producto.categoria.id)
            compra.save()
        except Exception as e:
            raise e
        return super(CompraCreationM, self).form_valid(form)


class CompraUpdate(UpdateView):
    model = Compra
    success_url = reverse_lazy('Compras:list')
    form_class = Compra_Form

class CompraDelete(DeleteView):
    model = Compra
    success_url = reverse_lazy('Compras:list')