from django.forms import ModelForm
from django import forms
from .models import Producto, Categoria
    

class Producto_Form(ModelForm):
    class Meta:
    	model = Producto
    	exclude = ('id', 'usuario')