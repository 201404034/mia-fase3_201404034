from django.conf.urls import url
from .views import (
	ColorDetail,
	ColorCreation,
	ColorUpdate,
	ColorDelete
)

urlpatterns = [
###############
#   Color    #
###############
    url(r'^(?P<pk>\d+)$', ColorDetail.as_view(), name='detail'),
    url(r'^nuevo/(?P<cantidad>\d+)$', ColorCreation.as_view(), name='new'),
    url(r'^editar/(?P<pk>\d+)$', ColorUpdate.as_view(), name='edit'),
    url(r'^borrar/(?P<pk>\d+)$', ColorDelete.as_view(), name='delete'),
]