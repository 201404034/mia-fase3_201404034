package usac.mia.manugr.fase_3;

import java.io.Serializable;

/**
 * Created by manugr on 8/11/16.
 */

public class Usuario implements Serializable {
    public String nombre,apellido,correo,direccion,telefono,genero,nacimiento,foto,rol;
    public Usuario(){
    this.nombre = "";
    }
    public Usuario(String nombre, String apellido, String correo, String direccion, String telefono, String genero, String nacimiento, String foto){
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.direccion = direccion;
        this.telefono = telefono;
        this.genero = genero;
        this.nacimiento = nacimiento;
        this.foto = foto;
    }
}
