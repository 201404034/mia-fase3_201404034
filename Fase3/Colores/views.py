from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)
from django.shortcuts import render
from .models import DetalleColor, Producto, Categoria

# Create your views here.
############################
#         Colores         #
############################
class ColorList(ListView):
    model = DetalleColor

class ColorDetail(DetailView):
    model = DetalleColor

class ColorCreation(CreateView):
    model = DetalleColor
    success_url = reverse_lazy('Productos:list')
    fields = [
        'color_nombre'
    ]
    def form_valid(self, form, **kwargs):
        try:
            col = form.save(commit=False)
            col.producto = Producto.objects.get(id = self.kwargs['cantidad'])
            col.categoria = Categoria.objects.get(id = col.producto.categoria.id)
            #col.save()
        except Exception as e:
            raise e
        return super(ColorCreation, self).form_valid(form)

class ColorUpdate(UpdateView):
    model = DetalleColor
    success_url = reverse_lazy('Productos:list')
    fields = [
        'color_nombre'
    ]

class ColorDelete(DeleteView):
    model = DetalleColor
    success_url = reverse_lazy('Productos:list')