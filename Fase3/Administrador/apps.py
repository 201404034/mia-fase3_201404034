from django.apps import AppConfig

class UsuariosConfig(AppConfig):
    name = 'Usuarios'

class PortadaConfig(AppConfig):
    name = 'Portada'