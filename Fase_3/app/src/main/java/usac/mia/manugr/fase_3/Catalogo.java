/**
 * Created by manugr on 9/11/16.
 */
package usac.mia.manugr.fase_3;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.sql.ResultSet;
import java.sql.Statement;


public class Catalogo extends ListActivity {
    private String[] productos;
    String query;
    private Class<?> miClase;
    Statement st;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        String result="";
        try{
            ConnectOra conn = new ConnectOra("10.0.2.2", "XE", "Fase_3","1234");
            st = conn.getConexion().createStatement();
            query = "SELECT * FROM Producto";
            ResultSet resultado = st.executeQuery(query);
            while(resultado.next()) {
                result += resultado.getString("Nombre") + "@";
            }
            resultado.close();
            st.close();
        }catch(Exception e) {
        }
        productos = result.split("@");
        setListAdapter(new ArrayAdapter<String>(Catalogo.this, android.R.layout.simple_list_item_1,productos ));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        try{
            ConnectOra conn = new ConnectOra("10.0.2.2", "XE", "Fase_3","1234");
            Statement st = conn.getConexion().createStatement();
            ResultSet resultado = st.executeQuery(query);
            if(resultado.next()){
                int contador = 0;
                while (contador < position){
                    resultado.next();
                    contador++;
                }
                Intent miIntento = new Intent(getApplicationContext(), Producto.class);
                Bundle cesta = new Bundle();
                String res = resultado.getString("ID");
                cesta.putString("ID",res);
                miIntento.putExtras(cesta);
                startActivity(miIntento);
            }
            resultado.close();
            st.close();
        }catch(Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
