package usac.mia.manugr.fase_3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Registro extends Activity {

    EditText correo, nombre, apellido, direccion, telefono, fecha;
    RadioButton macho, hembra;
    Button registrar, cancelar;
    Statement st;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        final String formattedDate = df.format(c.getTime());

        correo = (EditText) findViewById(R.id.et_Usuario_Registro);
        nombre = (EditText) findViewById(R.id.et_Nombre_Registro);
        apellido = (EditText) findViewById(R.id.et_Apellido_Registro);
        direccion = (EditText) findViewById(R.id.et_Direccion_Registro);
        telefono = (EditText) findViewById(R.id.et_Telefono_Registro);
        fecha = (EditText) findViewById(R.id.et_Fecha_Registro);

        macho = (RadioButton) findViewById(R.id.rb_Masculino);
        hembra = (RadioButton) findViewById(R.id.rb_Femenino);

        cancelar = (Button) findViewById(R.id.btn_Cancelar_Registro);
        cancelar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });

        registrar = (Button) findViewById(R.id.btn_Confirmar_Registro);
        registrar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    ConnectOra conn = new ConnectOra("10.0.2.2", "XE", "Fase_3", "1234");
                    st = conn.getConexion().createStatement();
                    String papaQuery = "Insert into Usuario VALUES (NULL," +
                            "'"+nombre.getText().toString()+"'," +
                            "'"+apellido.getText().toString()+"'," +
                            "'"+correo.getText().toString()+"'," +
                            telefono.getText().toString()+"," +
                            "'usu/default.png', ";
                    if(macho.isSelected()) {
                        papaQuery+="'Masculino',";
                    }else{
                        papaQuery+="'Femenino',";
                    }
                            papaQuery+="TO_DATE('"+fecha.getText().toString()+"','DD/MM/YYYY')," +
                                    "TO_DATE('"+formattedDate+"','dd/mm/yyyy')," +
                                    "'"+direccion.getText().toString()+"',1000,10,'D',1,NULL)";
                    ResultSet resultado = st.executeQuery(papaQuery);

                    if(resultado.rowInserted()){
                        Toast.makeText(getApplicationContext(), "Registrado exitosamente!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Algo salio mal", Toast.LENGTH_SHORT).show();
                    }
                    resultado.close();
                    st.close();
                    Intent i = new Intent(getApplicationContext(), Portada.class);
                    startActivity(i);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
