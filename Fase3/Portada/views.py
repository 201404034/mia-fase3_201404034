from django.shortcuts import render, get_object_or_404
from .models import Portada

# Create your views here.

def index_view(request):
	por = get_object_or_404(Portada, id=1)
	return render(request, "Portada/Normal.html", {"portada": por,})