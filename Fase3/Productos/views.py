from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (CreateView, UpdateView, DeleteView)
from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from .models import Producto, Usuario, Color, DetalleColor
from .forms import Producto_Form

# Create your views here.
############################
#         Productos        #
############################
class ProductoList(ListView):   
    model = Producto
    paginate_by = 10
    #usu = Usuario.objects.get(magia = User or None)
    #queryset = super(ProductoDetail, self).get_queryset()

class ProductoDetail(DetailView):
    model = Producto
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ProductoDetail, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['colores'] = DetalleColor.objects.filter(producto__id = self.kwargs['pk'])
        return context

class ProductoCreation(CreateView):
    model = Producto
    success_url = reverse_lazy('Productos:list')
    form_class = Producto_Form

    def form_valid(self, form):
        if self.request.method == 'POST':
            try:
                prod = form.save(commit=False)
                usu = Usuario.objects.get(magia = self.request.user)
                prod.usuario = usu
                #prod.save()
            except Exception as e:
                raise e
        return super(ProductoCreation, self).form_valid(form)

class ProductoUpdate(UpdateView):
    model = Producto
    success_url = reverse_lazy('Productos:list')
    form_class = Producto_Form

class ProductoDelete(DeleteView):
    model = Producto
    success_url = reverse_lazy('Productos:list')