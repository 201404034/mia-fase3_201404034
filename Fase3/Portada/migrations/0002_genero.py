# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Portada', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Genero',
            fields=[
                ('genero', models.CharField(max_length=10, serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'genero',
                'managed': False,
            },
        ),
    ]
