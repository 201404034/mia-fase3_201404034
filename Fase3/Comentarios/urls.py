from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import (
	ComentarioList,
	ComentarioDetail,
	ComentarioCreation,
	ComentarioUpdate,
	ComentarioDelete
)

urlpatterns = [
################
#  Comentario  #
################
	url(r'^$', ComentarioList.as_view(), name='list'),
    url(r'^(?P<pk>\d+)$', ComentarioDetail.as_view(), name='detail'),
    url(r'^nuevo$', ComentarioCreation.as_view(), name='new'),
    url(r'^editar/(?P<pk>\d+)$', ComentarioUpdate.as_view(), name='edit'),
    url(r'^borrar/(?P<pk>\d+)$', ComentarioDelete.as_view(), name='delete'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)