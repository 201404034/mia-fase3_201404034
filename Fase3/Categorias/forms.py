from django.forms import ModelForm
from django import forms
from .models import Categoria
    

class Categoria_Form(ModelForm):
    #nombre = forms.CharField()
    #descripcion = forms.CharField()
    #padre = forms.ModelChoiceField(queryset = Categoria.objects.all() or None)

    class Meta:
    	model = Categoria
    	fields = [
    		'nombre',
    		'descripcion',
    		'categoria'
    	]