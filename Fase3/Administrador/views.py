from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)
from django.shortcuts import render, get_object_or_404, redirect
from .forms import Portada_Form
from .models import Usuario, Portada, Rol

# Create your views here.
############################
#         Portada          #
############################
def index_view(request, pk):
    port = get_object_or_404(Portada, pk=pk)
    if request.method == "POST":
        form = Portada_Form(request.POST, instance=port)
        if form.is_valid():
            port = form.save(commit=False)
            port.save()
            return redirect('Admin:home', pk=1)
    else:
        form = Portada_Form(instance=port)
    return render(request, "Administrador/home.html", {'form':form})

def reportes_view(request, pk):
    help_macho = request.POST.get('year')
    administradoras = request.POST.get('year_a')
    disponibles = request.POST.get('cantidad')

    if help_macho:
        usuarios = Usuario.objects.filter(genero__genero='Masculino')
        usuarios = usuarios.filter(fecha_de_nacimiento >= kwargs['year'])
        context = {
            "usuarios": usuarios,
        }
        return render(request, "Reportes/Helpdesk_Machos.html", context)
    if administradoras:
        suarios = Usuario.objects.filter(genero__genero='Femenino')
        usuarios = usuarios.filter(fecha_de_nacimiento < kwargs['year'])
        context = {
            "usuarios": usuarios,
        }
        return render(request, "Reportes/Administradoras.html", context)
    if disponibles:
        productos = Producto.objects.filter(disponibles=kwargs['cantidad'])
        context = {
            "productos": productos,
        }
        return render(request, "Reportes/Productos_Disponibles.html", context)
    return render(request, "Administrador/reportes.html", {})

############################
#         Usuarios         #
############################

class UsuarioList(ListView):
    model = Usuario

class UsuarioDetail(DetailView):
    model = Usuario

class UsuarioCreation(CreateView):
    model = Usuario
    success_url = reverse_lazy('Admin:list')
    fields = [
        'nombre',
        'apellido',
        'correo',
        'telefono',
        'foto',
        'genero',
        'fecha_de_nacimineto',
        'direccion',
        'credito_disponible',
        'ganancia_obtenida',
        'clase_cliente',
        'rol',
        'magia'
    ]

class UsuarioUpdate(UpdateView):
    model = Usuario
    success_url = reverse_lazy('Admin:list')
    fields = [
        'nombre',
        'apellido',
        'correo',
        'telefono',
        'foto',
        'genero',
        'fecha_de_nacimineto',
        'direccion',
        'credito_disponible',
        'ganancia_obtenida',
        'clase_cliente',
        'rol',
        'magia',
    ]

class UsuarioDelete(DeleteView):
    model = Usuario
    success_url = reverse_lazy('Admin:list')