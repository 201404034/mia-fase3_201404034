# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bitacora',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('fecha', models.DateField(auto_now_add=True)),
                ('accion', models.CharField(max_length=500)),
            ],
            options={
                'db_table': 'bitacora',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Carrito',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'carrito',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=50)),
                ('descripcion', models.CharField(max_length=1500)),
            ],
            options={
                'db_table': 'categoria',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Color',
            fields=[
                ('nombre', models.CharField(max_length=20, serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'color',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('titulo', models.CharField(max_length=25)),
                ('fecha', models.DateField(auto_now_add=True)),
                ('ponderacion', models.BigIntegerField()),
                ('cuerpo', models.CharField(max_length=1500)),
            ],
            options={
                'db_table': 'comentario',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Compra',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('cantidad', models.BigIntegerField()),
            ],
            options={
                'db_table': 'compra',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DetalleColor',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
            ],
            options={
                'db_table': 'detalle_color',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Factura',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('fecha', models.DateField(auto_now_add=True)),
            ],
            options={
                'db_table': 'factura',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Portada',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=50)),
                ('eslogan', models.CharField(max_length=250)),
                ('logo', models.ImageField(upload_to='portada/')),
                ('video', models.FileField(upload_to='videos/')),
                ('mision', models.CharField(max_length=1500)),
                ('vision', models.CharField(max_length=1500)),
                ('about_me', models.CharField(max_length=1500)),
            ],
            options={
                'db_table': 'portada',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Producto',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=50)),
                ('imagen', models.ImageField(max_length=500, upload_to='prod/')),
                ('descripcion', models.CharField(max_length=1500)),
                ('precio', models.DecimalField(max_digits=10, decimal_places=2)),
                ('fecha_de_publicacion', models.DateField(auto_now_add=True)),
                ('disponibles', models.BigIntegerField()),
            ],
            options={
                'db_table': 'producto',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Puntuacion',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('valor', models.BigIntegerField()),
                ('comentario', models.CharField(max_length=250, null=True, blank=True)),
            ],
            options={
                'db_table': 'puntuacion',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Rol',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=25)),
            ],
            options={
                'db_table': 'rol',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.BigIntegerField(serialize=False, primary_key=True)),
                ('nombre', models.CharField(max_length=25)),
                ('apellido', models.CharField(max_length=25)),
                ('password', models.CharField(max_length=50)),
                ('correo', models.EmailField(max_length=50)),
                ('telefono', models.CharField(max_length=12, null=True, blank=True)),
                ('foto', models.ImageField(max_length=500, upload_to='usu/')),
                ('genero', models.CharField(max_length=10)),
                ('fecha_de_nacimineto', models.DateField()),
                ('fecha_de_registro', models.DateField(auto_now_add=True)),
                ('direccion', models.CharField(max_length=500)),
                ('credito_disponible', models.DecimalField(max_digits=12, decimal_places=2)),
                ('ganancia_obtenida', models.DecimalField(max_digits=12, decimal_places=2)),
                ('clase_cliente', models.CharField(max_length=1)),
            ],
            options={
                'db_table': 'usuario',
                'managed': False,
            },
        ),
    ]
