from django.shortcuts import render, get_object_or_404, redirect
from .models import Usuario, Rol, Puntuacion, Producto, Compra, Categoria, Portada
from django.db.models import Count
from django.http import HttpResponse
from datetime import datetime

from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, cm

# Create your views here.
def Promedio_Helpdesk(request):
	usuarios = Usuario.objects.filter(rol=2)
	context = {
		"usuarios": usuarios,
	}
	return render(request, "Reportes/Helpdesk_Promedio.html", context)

def Machos_Helpdesk(request):
	if request.method == 'POST':
		usuarios = Usuario.objects.filter(genero__genero='Masculino')
		usuarios = usuarios.filter(fecha_de_nacimiento >= request.GET.get('year'))
		context = {
			"usuarios": usuarios,
		}
	else:
		context = {}
	return render(request, "Reportes/Helpdesk_Machos.html", context)

def Administradoras(request):
	if self.request.method == 'POST':
		usuarios = Usuario.objects.filter(genero__genero='Femenino')
		usuarios = usuarios.filter(fecha_de_nacimiento < request.GET.get('year'))
		context = {
			"usuarios": usuarios,
		}
	else:
		context = {}
	return render(request, "Reportes/Administradoras.html", context)

def Fichudos(request):
	usuarios = Usuario.objects.filter(rol__id=1).order_by('-ganancia_obtenida')
	context = {
		"usuarios": usuarios,
	}
	return render(request, "Reportes/Fichudos.html", context)

def Promedio_Productos(request):
	productos = Producto.objects.all()
	context = {
		"productos": productos,
	}
	return render(request, "Reportes/Productos_Promedio.html", context)

def Top_Productos(request):
	compras = Compra.objects.values('producto__nombre', 'producto__imagen').annotate(prod_count=Count('producto__nombre')).order_by('-prod_count')[:3]
	context = {
		"productos": compras,
	}
	return render(request, "Reportes/Productos_Top.html", context)

def Top_Vendedores(request):
	usuarios = Producto.objects.values('usuario__nombre', 'usuario__correo').annotate(usu_count=Count('usuario__nombre')).order_by('-usu_count')[:3]
	context = {
		"usuarios": usuarios,
	}
	return render(request, "Reportes/Vendedores_Top.html", context)

def Productos_Lista(request):
	productos = Producto.objects.all()
	subcategorias = Categoria.objects.exclude(Padre__isnull=True)
	context = {
		"productos": productos,
		"subcategorias": subcategorias,
	}
	return render(request, "Reportes/Productos_Lista.html", context)

def Productos_Disponibles(request):
	if request.method == 'POST':
		productos = Producto.objects.filter(disponibles=request.GET.get('cantidad'))
		context = {
			"productos": productos,
		}
	else:
		context = {}
	return render(request, "Reportes/Productos_Disponibles.html", context)

def Promedio_Helpdesk_pdf(request):
	portada = get_object_or_404(Portada, pk=1)
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename=' + portada.nombre.replace(" ", "") + '-reporte2.pdf'
	
	buffer = BytesIO()
	p = canvas.Canvas(buffer, pagesize=A4)
	
	p.setLineWidth(.3)
	p.setFont('Helvetica',22)
	p.drawString(30,750,portada.nombre)
	p.setFont('Helvetica',12)
	p.drawString(30,735,'Reporte De Usuarios Helpdesk Y Su Promedio')

	p.drawString(100,700,'Helpdesk')
	p.drawString(250,700,'Promedio')
	line = 680

	helpdseks = Usuario.objects.filter(rol__id=2)

	listado = [] 
	for usu in helpdseks:
		listado.append((str(usu.correo), "0.0" ))

	for campo in listado:
		p.setFont('Helvetica',12)
		p.drawString(80, line, str(campo[0]))
		p.drawString(245, line, str(campo[1]))

		line = line - 18
		if line == 50:
			
			p.setLineWidth(.3)
			p.setFont('Helvetica',22)
			p.drawString(30,750,portada.nombre)
			p.setFont('Helvetica',12)
			p.drawString(30,735,'Reporte De Usuarios Helpdesk Y Su Promedio')
			now = datetime.now() 
			fecha = now.strftime("%H:%M %d-%m-%Y") 
			p.setFont('Helvetica-Bold',12)
			p.drawString(460,750,fecha)
			p.line(460,747,560,747)
			
			p.drawString(100,700,'Helpdesk')
			p.drawString(250,700,'Promedio')
			line = 680
			p.showpage()

	
	p.save()
	pdf = buffer.getvalue()
	buffer.close()
	response.write(pdf)
	return response

def Machos_Helpdesk_pdf(request, **kwargs):
	portada = get_object_or_404(Portada, pk=1)
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename=' + portada.nombre.replace(" ", "") + '-reporte2.pdf'
	
	buffer = BytesIO()
	p = canvas.Canvas(buffer, pagesize=A4)

	
	p.setLineWidth(.3)
	p.setFont('Helvetica',22)
	p.drawString(30,750,portada.nombre)
	p.setFont('Helvetica',12)
	p.drawString(30,735,'Reporte De Usuarios Helpdesk De Sexo Masculino')

	now = datetime.now() 
	fecha = now.strftime("%H:%M %d-%m-%Y") 
	p.setFont('Helvetica-Bold',12)
	p.drawString(460,750,fecha)
	p.line(460,747,560,747)
	
	p.drawString(100,700,'Helpdesk')
	p.drawString(250,700,'Sexo')
	p.drawString(400,700,'Fecha de Nacimiento')
	line = 680

	admins = Usuario.objects.filter(rol__id=2)
	admins = admins.filter(genero__genero='Masculino')
	fecha = request.GET.get("fecha")
	if fecha:
		admins = admins.filter(fecha_de_nacimiento__range=["1000-01-31", fecha])

	listado = [] 
	for admin in admins:
		listado.append((str(admin.correo), str(admin.genero), str(admin.fecha_de_nacimiento)))

	for campo in listado:
		p.setFont('Helvetica',12)
		p.drawString(80, line, str(campo[0]))
		p.drawString(245, line, str(campo[1]))
		p.drawString(430, line, str(campo[2]))

		line = line - 18
		if line == 50:
			
			p.setLineWidth(.3)
			p.setFont('Helvetica',22)
			p.drawString(30,750,portada.nombre)
			p.setFont('Helvetica',12)
			p.drawString(30,735,'Reporte De Usuarios Helpdesk De Sexo Masculino')
			now = datetime.now() 
			fecha = now.strftime("%H:%M %d-%m-%Y") 
			p.setFont('Helvetica-Bold',12)
			p.drawString(460,750,fecha)
			p.line(460,747,560,747)
			
			p.drawString(100,700,'Helpdesk')
			p.drawString(250,700,'Sexo')
			p.drawString(400,700,'Fecha de Nacimiento')
			line = 680
			p.showpage()

	
	p.save()
	pdf = buffer.getvalue()
	buffer.close()
	response.write(pdf)
	return response

def Administradoras_pdf(request, **kwargs):
	portada = get_object_or_404(Portada, pk=1)
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename=' + portada.nombre.replace(" ", "") + '-reporte2.pdf'
	
	buffer = BytesIO()
	p = canvas.Canvas(buffer, pagesize=A4)

	
	p.setLineWidth(.3)
	p.setFont('Helvetica',22)
	p.drawString(30,750,portada.nombre)
	p.setFont('Helvetica',12)
	p.drawString(30,735,'Reporte De Usuarios Administradores De Sexo Femenino')

	now = datetime.now() 
	fecha = now.strftime("%H:%M %d-%m-%Y") 
	p.setFont('Helvetica-Bold',12)
	p.drawString(460,750,fecha)
	p.line(460,747,560,747)

	
	p.drawString(100,700,'Administradora')
	p.drawString(250,700,'Sexo')
	p.drawString(400,700,'Fecha de Nacimiento')
	line = 680

	admins = Usuario.objects.filter(rol__id=3)
	admins = admins.filter(genero__genero='Femenino')
	fecha = request.GET.get("fecha")
	if fecha:
		admins = admins.filter(fecha_de_nacimiento__range=["1000-01-31", fecha])

	listado = [] 
	for admin in admins:
		listado.append((str(admin.correo), str(admin.genero), str(admin.fecha_de_nacimiento)))

	for campo in listado:
		p.setFont('Helvetica',12)
		p.drawString(80, line, str(campo[0]))
		p.drawString(245, line, str(campo[1]))
		p.drawString(430, line, str(campo[2]))

		line = line - 18
		if line == 50:
			
			p.setLineWidth(.3)
			p.setFont('Helvetica',22)
			p.drawString(30,750,portada.nombre)
			p.setFont('Helvetica',12)
			p.drawString(30,735,'Reporte Administradores Sexo Femenino')
			now = datetime.now() 
			fecha = now.strftime("%H:%M %d-%m-%Y") 
			p.setFont('Helvetica-Bold',12)
			p.drawString(460,750,fecha)
			p.line(460,747,560,747)
			
			p.drawString(100,700,'Administradora')
			p.drawString(250,700,'Sexo')
			p.drawString(400,700,'Fecha de Nacimiento')
			line = 680
			p.showpage()

	
	p.save()
	pdf = buffer.getvalue()
	buffer.close()
	response.write(pdf)
	return response

def Fichudos_pdf(request):
	portada = get_object_or_404(Portada, pk=1)
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename=' + portada.nombre.replace(" ", "") + '-reporte2.pdf'
	
	buffer = BytesIO()
	p = canvas.Canvas(buffer, pagesize=A4)
	
	p.setLineWidth(.3)
	p.setFont('Helvetica',22)
	p.drawString(30,750,portada.nombre)
	p.setFont('Helvetica',12)
	p.drawString(30,735,'Reporte De Clientes Con Mayores Ganancias')

	now = datetime.now() 
	fecha = now.strftime("%H:%M %d-%m-%Y") 
	p.setFont('Helvetica-Bold',12)
	p.drawString(460,750,fecha)
	p.line(460,747,560,747)

	
	p.drawString(100,700,'Cliente')
	p.drawString(250,700,'Cantidad')
	line = 680

	admins = Usuario.objects.filter(rol__id=1).order_by('-ganancia_obtenida')

	listado = [] 
	for admin in admins:
		listado.append((str(admin.correo), str(admin.ganancia_obtenida)))

	for campo in listado:
		p.setFont('Helvetica',12)
		p.drawString(80, line, str(campo[0]))
		p.drawString(245, line, str(campo[1]))

		line = line - 18
		if line == 50:
			
			p.setLineWidth(.3)
			p.setFont('Helvetica',22)
			p.drawString(30,750,portada.nombre)
			p.setFont('Helvetica',12)
			p.drawString(30,735,'Reporte De Clientes Con Mayores Ganancias')
			now = datetime.datetime.now()
			fecha = now.strftime("%H:%M %d-%m-%Y") 
			p.setFont('Helvetica-Bold',12)
			p.drawString(460,750,fecha)
			p.line(460,747,560,747)
			
			p.drawString(100,700,'Cliente')
			p.drawString(250,700,'Ganancias')
			line = 680
			p.showpage()

	
	p.save()
	pdf = buffer.getvalue()
	buffer.close()
	response.write(pdf)
	return response

def Promedio_Productos_pdf(request):
	portada = get_object_or_404(Portada, pk=1)
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename=' + portada.nombre.replace(" ", "") + '-reporte2.pdf'
	
	buffer = BytesIO()
	p = canvas.Canvas(buffer, pagesize=A4)

	
	p.setLineWidth(.3)
	p.setFont('Helvetica',22)
	p.drawString(30,750,portada.nombre)
	p.setFont('Helvetica',12)
	p.drawString(30,735,'Reporte De Productos Y Su Puntuacion Promedio')

	now = datetime.now() 
	fecha = now.strftime("%H:%M %d-%m-%Y") 
	p.setFont('Helvetica-Bold',12)
	p.drawString(460,750,fecha)
	p.line(460,747,560,747)

	
	p.drawString(100,700,'Producto')
	p.drawString(250,700,'Promedio')
	line = 680

	admins = Productos.objects.all()

	listado = [] 
	for admin in admins:
		listado.append((str(admin.nombre), str("0.0")))

	for campo in listado:
		p.setFont('Helvetica',12)
		p.drawString(80, line, str(campo[0]))
		p.drawString(245, line, str(campo[1]))

		line = line - 18
		if line == 50:
			
			p.setLineWidth(.3)
			p.setFont('Helvetica',22)
			p.drawString(30,750,portada.nombre)
			p.setFont('Helvetica',12)
			p.drawString(30,735,'Reporte De Productos Y Su Puntuacion Promedio')
			now = datetime.datetime.now()
			fecha = now.strftime("%H:%M %d-%m-%Y") 
			p.setFont('Helvetica-Bold',12)
			p.drawString(460,750,fecha)
			p.line(460,747,560,747)
			
			p.drawString(100,700,'Producto')
			p.drawString(250,700,'Promedio')
			line = 680
			p.showpage()

	
	p.save()
	pdf = buffer.getvalue()
	buffer.close()
	response.write(pdf)
	return response

def Top_Productos_pdf(request):
	portada = get_object_or_404(Portada, pk=1)
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename=' + portada.nombre.replace(" ", "") + '-reporte2.pdf'
	
	buffer = BytesIO()
	p = canvas.Canvas(buffer, pagesize=A4)

	
	p.setLineWidth(.3)
	p.setFont('Helvetica',22)
	p.drawString(30,750,portada.nombre)
	p.setFont('Helvetica',12)
	p.drawString(30,735,'Reporte Top 3 De Productos Mas Vendidos')

	now = datetime.now() 
	fecha = now.strftime("%H:%M %d-%m-%Y") 
	p.setFont('Helvetica-Bold',12)
	p.drawString(460,750,fecha)
	p.line(460,747,560,747)

	
	p.drawString(100,700,'Producto')
	line = 680

	admins = Compra.objects.values('producto__nombre', 'producto__imagen').annotate(prod_count=Count('producto__nombre')).order_by('-prod_count')[:3]

	listado = [] 
	for admin in admins:
		listado.append((str(admin.nombre)))

	for campo in listado:
		p.setFont('Helvetica',12)
		p.drawString(80, line, str(campo[0]))

		line = line - 18
		if line == 50:
			
			p.setLineWidth(.3)
			p.setFont('Helvetica',22)
			p.drawString(30,750,portada.nombre)
			p.setFont('Helvetica',12)
			p.drawString(30,735,'Reporte Top 3 De Productos Mas Vendidos')
			now = datetime.datetime.now()
			fecha = now.strftime("%H:%M %d-%m-%Y") 
			p.setFont('Helvetica-Bold',12)
			p.drawString(460,750,fecha)
			p.line(460,747,560,747)
			
			p.drawString(100,700,'Producto')
			line = 680
			p.showpage()

	
	p.save()
	pdf = buffer.getvalue()
	buffer.close()
	response.write(pdf)
	return response

def Top_Vendedores_pdf(request):
	portada = get_object_or_404(Portada, pk=1)
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename=' + portada.nombre.replace(" ", "") + '-reporte2.pdf'
	
	buffer = BytesIO()
	p = canvas.Canvas(buffer, pagesize=A4)

	
	p.setLineWidth(.3)
	p.setFont('Helvetica',22)
	p.drawString(30,750,portada.nombre)
	p.setFont('Helvetica',12)
	p.drawString(30,735,'Reporte Top 3 De Usuarios Con Mas Productos')

	now = datetime.now() 
	fecha = now.strftime("%H:%M %d-%m-%Y") 
	p.setFont('Helvetica-Bold',12)
	p.drawString(460,750,fecha)
	p.line(460,747,560,747)

	
	p.drawString(100,700,'Cliente')
	p.drawString(250,700,'Promedio')
	line = 680

	admins = Producto.objects.values('usuario__nombre', 'usuario__correo').annotate(usu_count=Count('usuario__nombre')).order_by('-usu_count')[:3]

	listado = [] 
	for admin in admins:
		listado.append((str(admin.nombre), str(admin.usu_count)))

	for campo in listado:
		p.setFont('Helvetica',12)
		p.drawString(80, line, str(campo[0]))
		p.drawString(245, line, str(campo[1]))

		line = line - 18
		if line == 50:
			
			p.setLineWidth(.3)
			p.setFont('Helvetica',22)
			p.drawString(30,750,portada.nombre)
			p.setFont('Helvetica',12)
			p.drawString(30,735,'Reporte Top 3 De Usuarios Con Mas Productos')
			now = datetime.datetime.now()
			fecha = now.strftime("%H:%M %d-%m-%Y") 
			p.setFont('Helvetica-Bold',12)
			p.drawString(460,750,fecha)
			p.line(460,747,560,747)
			
			p.drawString(100,700,'Cliente')
			p.drawString(250,700,'Promedio')
			line = 680
			p.showpage()

	
	p.save()
	pdf = buffer.getvalue()
	buffer.close()
	response.write(pdf)
	return response

def Productos_Lista_pdf(request):
	portada = get_object_or_404(Portada, pk=1)
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename=' + portada.nombre.replace(" ", "") + '-reporte2.pdf'
	
	buffer = BytesIO()
	p = canvas.Canvas(buffer, pagesize=A4)

	
	p.setLineWidth(.3)
	p.setFont('Helvetica',22)
	p.drawString(30,750,portada.nombre)
	p.setFont('Helvetica',12)
	p.drawString(30,735,'Reporte Listado De Productos Y Su Categoria')

	now = datetime.now() 
	fecha = now.strftime("%H:%M %d-%m-%Y") 
	p.setFont('Helvetica-Bold',12)
	p.drawString(460,750,fecha)
	p.line(460,747,560,747)

	
	p.drawString(100,700,'Producto')
	p.drawString(250,700,'Categoria')
	line = 680

	admins = Producto.objects.all()

	listado = [] 
	for admin in admins:
		listado.append((str(admin.nombre), str(admin.categoria)))

	for campo in listado:
		p.setFont('Helvetica',12)
		p.drawString(80, line, str(campo[0]))
		p.drawString(245, line, str(campo[1]))

		line = line - 18
		if line == 50:
			
			p.setLineWidth(.3)
			p.setFont('Helvetica',22)
			p.drawString(30,750,portada.nombre)
			p.setFont('Helvetica',12)
			p.drawString(30,735,'Reporte Listado De Productos Y Su Categoria')
			now = datetime.datetime.now()
			fecha = now.strftime("%H:%M %d-%m-%Y") 
			p.setFont('Helvetica-Bold',12)
			p.drawString(460,750,fecha)
			p.line(460,747,560,747)
			
			p.drawString(100,700,'Producto')
			p.drawString(250,700,'Categoria')
			line = 680
			p.showpage()

	
	p.save()
	pdf = buffer.getvalue()
	buffer.close()
	response.write(pdf)
	return response

def Productos_Disponibles_pdf(request, **kwargs):
	portada = get_object_or_404(Portada, pk=1)
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename=' + portada.nombre.replace(" ", "") + '-reporte2.pdf'
	
	buffer = BytesIO()
	p = canvas.Canvas(buffer, pagesize=A4)

	p.setLineWidth(.3)
	p.setFont('Helvetica',22)
	p.drawString(30,750,portada.nombre)
	p.setFont('Helvetica',12)
	p.drawString(30,735,'Reporte De Productos Disponibles')

	now = datetime.now() 
	fecha = now.strftime("%H:%M %d-%m-%Y") 
	p.setFont('Helvetica-Bold',12)
	p.drawString(460,750,fecha)
	p.line(460,747,560,747)

	
	p.drawString(100,700,'Producto')
	p.drawString(250,700,'Cantidad')
	line = 680

	admins = Producto.objects.all()
	cantidad = request.GET.get("cantidad")
	if cantidad:
		admins = admins.filter(cantidad=cantidad)

	listado = [] 
	for admin in admins:
		listado.append((str(admin.nombre), str(admin.disponibles)))

	for campo in listado:
		p.setFont('Helvetica',12)
		p.drawString(80, line, str(campo[0]))
		p.drawString(245, line, str(campo[1]))

		line = line - 18
		if line == 50:
			
			p.setLineWidth(.3)
			p.setFont('Helvetica',22)
			p.drawString(30,750,portada.nombre)
			p.setFont('Helvetica',12)
			p.drawString(30,735,'Reporte De Productos Disponibles')
			now = datetime.now() 
			fecha = now.strftime("%H:%M %d-%m-%Y") 
			p.setFont('Helvetica-Bold',12)
			p.drawString(460,750,fecha)
			p.line(460,747,560,747)
			
			p.drawString(100,700,'Producto')
			p.drawString(250,700,'Cantidad')
			line = 680
			p.showpage()

	
	p.save()
	pdf = buffer.getvalue()
	buffer.close()
	response.write(pdf)
	return response
