from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import index_view

urlpatterns = [
	url(r'^$', index_view, name='home'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)