from django.contrib import admin
from .models import (Portada,
	Usuario,
	Rol, 
	Puntuacion, 
	Producto, 
	Genero, 
	Factura, 
	DetalleColor, 
	Compra, 
	Comentario, 
	Color, 
	Categoria, 
	Carrito, 
	Bitacora
)

# Register your models here.
@admin.register(Portada)
class PortadaAdmin(admin.ModelAdmin):
    pass

@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    pass

@admin.register(Rol)
class RolAdmin(admin.ModelAdmin):
    pass
    
@admin.register(Puntuacion)
class PuntuacionAdmin(admin.ModelAdmin):
    pass
    
@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
    pass
    
@admin.register(Genero)
class GeneroAdmin(admin.ModelAdmin):
    pass
    
@admin.register(Factura)
class FacturaAdmin(admin.ModelAdmin):
    pass
    
@admin.register(DetalleColor)
class DetalleColorAdmin(admin.ModelAdmin):
    pass
    
@admin.register(Compra)
class CompraAdmin(admin.ModelAdmin):
    pass
    
@admin.register(Comentario)
class ComentarioAdmin(admin.ModelAdmin):
    pass
    
@admin.register(Color)
class ColorAdmin(admin.ModelAdmin):
    pass
    
@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    pass
    
@admin.register(Carrito)
class CarritoAdmin(admin.ModelAdmin):
    pass
    
@admin.register(Bitacora)
class BitacoraAdmin(admin.ModelAdmin):
    pass