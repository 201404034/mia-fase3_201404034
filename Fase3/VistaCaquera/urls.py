from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import (
	Bonito_View
)

urlpatterns = [
	url(r'^$', Bonito_View, name='home'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)