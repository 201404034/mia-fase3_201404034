package usac.mia.manugr.fase_3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Statement;

public class UpdatePortada extends Activity {
    EditText nombre, eslogan, mision, vision, about;
    Button actualizar;
    Statement st;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_portada);

        nombre = (EditText) findViewById(R.id.et_Nombre_Update);
        eslogan = (EditText) findViewById(R.id.et_Eslogan_Update);
        mision = (EditText) findViewById(R.id.et_Mision_Update);
        vision = (EditText) findViewById(R.id.et_Vision_Update);
        about = (EditText) findViewById(R.id.et_About_Update);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            ConnectOra conn = new ConnectOra("10.0.2.2", "XE", "Fase_3", "1234");
            st = conn.getConexion().createStatement();
            ResultSet resultado = st.executeQuery("SELECT * From PORTADA WHERE ID = 1");

            if(resultado.next())
            {
                nombre.setText(resultado.getString("NOMBRE"));
                mision.setText(resultado.getString("MISION"));
                vision.setText(resultado.getString("VISION"));
                eslogan.setText(resultado.getString("ESLOGAN"));
                about.setText(resultado.getString("ABOUT_ME"));
            }
            resultado.close();
            st.close();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        actualizar = (Button) findViewById(R.id.btn_ActualizarPortada);
        actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ConnectOra conn = new ConnectOra("10.0.2.2", "XE", "Fase_3", "1234");
                    st = conn.getConexion().createStatement();
                    String papaQuery = "UPDATE Portada " +
                            "SET ESLOGAN = '"+eslogan.getText().toString()+"'," +
                            " NOMBRE = '"+nombre.getText().toString()+"'," +
                            " MISION = '"+mision.getText().toString()+"', " +
                            " VISION = '"+vision.getText().toString()+"', " +
                            " ABOUT_ME='"+about.getText().toString()+"' WHERE ID = 1";
                    ResultSet resultado = st.executeQuery(papaQuery);

                    if(resultado.rowUpdated()){
                        Toast.makeText(getApplicationContext(), "Actualizado exitosamente!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Algo salio mal", Toast.LENGTH_SHORT).show();
                    }
                    resultado.close();
                    st.close();
                    Intent i = new Intent(getApplicationContext(), Portada.class);
                    startActivity(i);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
