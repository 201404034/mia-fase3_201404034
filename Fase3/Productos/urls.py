from django.conf.urls import url
from .views import (
	ProductoList,
	ProductoDetail,
	ProductoCreation,
	ProductoUpdate,
	ProductoDelete
)

urlpatterns = [
###############
#  Producto  #
###############
	url(r'^$', ProductoList.as_view(), name='list'),
    url(r'^(?P<pk>\d+)$', ProductoDetail.as_view(), name='detail'),
    url(r'^nuevo$', ProductoCreation.as_view(), name='new'),
    url(r'^editar/(?P<pk>\d+)$', ProductoUpdate.as_view(), name='edit'),
    url(r'^borrar/(?P<pk>\d+)$', ProductoDelete.as_view(), name='delete'),
]