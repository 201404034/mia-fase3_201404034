# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models

class Portada(models.Model):
    id = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=50)
    eslogan = models.CharField(max_length=250)
    logo = models.ImageField(upload_to='portada/')
    video = models.FileField(upload_to='videos/')
    mision = models.CharField(max_length=1500)
    vision = models.CharField(max_length=1500)
    about_me = models.CharField(max_length=1500)

    def __unicode__(self):
        return unicode(self.nombre) or u''

    class Meta:
        managed = False
        db_table = 'portada'

