from django import forms
from .models import Portada

class Portada_Form(forms.ModelForm):
	class Meta:
		model = Portada
		fields =(
				"eslogan",
    			"logo",
    			"video",
    			"mision",
    			"vision",
    			"about_me"
			)