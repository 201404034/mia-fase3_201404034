package usac.mia.manugr.fase_3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Statement;

public class MenuCrud extends Activity {

    TextView nombre, genero, fecha, correo;
    EditText apellido, direccion, telefono;
    ImageView foto;
    Button eliminar, actualizar;
    Statement st;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_crud);

        nombre = (TextView) findViewById(R.id.et_Nombre_Crud);
        apellido = (EditText) findViewById(R.id.et_Apellido_Crud);
        direccion = (EditText) findViewById(R.id.et_Direccion_Crud);
        telefono = (EditText) findViewById(R.id.et_Telefono_Crud);
        genero = (TextView) findViewById(R.id.et_Genero_Crud);
        fecha = (TextView) findViewById(R.id.et_Fecha_Crud);
        correo = (TextView) findViewById(R.id.txt_Correo_Crud);

        foto = (ImageView) findViewById(R.id.Foto_Crud);

        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("ID");

        try{
            ConnectOra conn = new ConnectOra("10.0.2.2", "XE", "Fase_3","1234");
            Statement st = conn.getConexion().createStatement();
            ResultSet resultado = st.executeQuery("SELECT * FROM Usuario WHERE ID = "+id);
            if(resultado.next()){
                nombre.setText(resultado.getString("NOMBRE"));
                apellido.setText(resultado.getString("APELLIDO"));
                correo.setText(resultado.getString("CORREO"));
                direccion.setText(resultado.getString("DIRECCION"));
                telefono.setText(resultado.getString("TELEFONO"));
                fecha.setText(resultado.getString("FECHAD_DE_NACIMIENTO"));
                if("1".equals(resultado.getString("GENERO_ID"))){
                    genero.setText("Masculino");
                }else{
                    genero.setText("Femenino");
                }

                String path = "http://10.0.2.2:8000/media/"+resultado.getString("FOTO");
                URL url = new URL(path);
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                foto.setImageBitmap(bmp);
            }
            resultado.close();
            st.close();
        }catch(Exception e) {
        }


        actualizar = (Button) findViewById(R.id.btn_Actualizar_Crud);
        actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ConnectOra conn = new ConnectOra("10.0.2.2", "XE", "Fase_3", "1234");
                    st = conn.getConexion().createStatement();
                    String papaQuery = "UPDATE Usuario " +
                            "SET APELLIDO = '"+apellido.getText().toString()+"'," +
                            " DIRECCION = '"+direccion.getText().toString()+"'," +
                            " TELEFONO = '"+telefono.getText().toString()+"' " +
                            " WHERE id = "+id;
                    ResultSet resultado = st.executeQuery(papaQuery);

                    if(resultado.rowUpdated()){
                        Toast.makeText(getApplicationContext(), "Actualizado exitosamente!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Algo salio mal", Toast.LENGTH_SHORT).show();
                    }
                    resultado.close();
                    st.close();
                    Intent i = new Intent(getApplicationContext(), Administrador.class);
                    startActivity(i);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        eliminar = (Button) findViewById(R.id.btn_Eliminar_Crud);
        eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ConnectOra conn = new ConnectOra("10.0.2.2", "XE", "Fase_3", "1234");
                    st = conn.getConexion().createStatement();
                    String papaQuery = "DELETE FROM Usuario " +
                            " WHERE ID = "+id;
                    ResultSet resultado = st.executeQuery(papaQuery);

                    if(resultado.rowDeleted()){
                        Toast.makeText(getApplicationContext(), "Eliminado exitosamente!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(), "Algo salio mal", Toast.LENGTH_SHORT).show();
                    }
                    resultado.close();
                    st.close();
                    Intent i = new Intent(getApplicationContext(), Administrador.class);
                    startActivity(i);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
