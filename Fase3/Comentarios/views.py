from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)
from django.shortcuts import render, get_object_or_404, redirect
from .models import Comentario

# Create your views here.

############################
#         Comentarios         #
############################
class ComentarioList(ListView):
    model = Comentario

class ComentarioDetail(DetailView):
    model = Comentario

class ComentarioCreation(CreateView):
    model = Comentario
    success_url = reverse_lazy('Productos:list')
    fields = [
        'titulo',
        'ponderacion',
        'cuerpo',
        'compra',
        'comentario'
    ]

class ComentarioUpdate(UpdateView):
    model = Comentario
    success_url = reverse_lazy('Productos:list')
    fields = [
        'titulo',
        'ponderacion',
        'cuerpo',
        'compra',
        'comentario'
    ]

class ComentarioDelete(DeleteView):
    model = Comentario
    success_url = reverse_lazy('Productos:list')