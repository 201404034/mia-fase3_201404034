from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models

class Carrito(models.Model):
    id = models.BigIntegerField(primary_key=True)
    usuario = models.ForeignKey('Usuario')

    def __unicode__(self):
        return unicode(self.id) or u''

    class Meta:
        managed = False
        db_table = 'carrito'


class Categoria(models.Model):
    id = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=1500)
    categoria = models.ForeignKey('self', blank=True, null=True, related_name='Padre')

    def __unicode__(self):
        return unicode(self.nombre) or u''

    class Meta:
        managed = False
        db_table = 'categoria'


class Compra(models.Model):
    id = models.BigIntegerField(primary_key=True)
    cantidad = models.BigIntegerField()
    carrito = models.ForeignKey(Carrito)
    producto = models.ForeignKey('Producto', related_name='compra_producto')
    categoria = models.ForeignKey('Categoria',   related_name='compra_categoria')


    def __unicode__(self):
        return unicode(self.id) or u''

    class Meta:
        managed = False
        db_table = 'compra'

class Factura(models.Model):
    id = models.BigIntegerField(primary_key=True)
    fecha = models.DateField(auto_now_add=True)
    carrito = models.ForeignKey(Carrito)

    def __unicode__(self):
        return unicode(self.id) or u''

    class Meta:
        managed = False
        db_table = 'factura'

class Genero(models.Model):
    genero = models.CharField(primary_key=True, max_length=10)

    def __unicode__(self):
        return unicode(self.genero) or u''

    class Meta:
        managed = False
        db_table = 'genero'

class Rol(models.Model):
    id = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=25)

    def __unicode__(self):
        return unicode(self.nombre) or u''

    class Meta:
        managed = False
        db_table = 'rol'

class Producto(models.Model):
    id = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=50)
    imagen = models.ImageField(upload_to='prod/')
    descripcion = models.CharField(max_length=1500)
    precio = models.DecimalField(max_digits=10, decimal_places=2)
    fecha_de_publicacion = models.DateField(auto_now_add=True)
    disponibles = models.BigIntegerField()
    categoria = models.ForeignKey(Categoria)
    usuario = models.ForeignKey('Usuario', related_name='provedor')

    def __unicode__(self):
        return unicode(self.nombre) or u''

    class Meta:
        managed = False
        db_table = 'producto'

class Usuario(models.Model):
    magia = models.OneToOneField(User, related_name='user_Comp')
    id = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=25)
    apellido = models.CharField(max_length=25)
    correo = models.EmailField(max_length=50)
    telefono = models.CharField(max_length=12, blank=True, null=True)
    foto = models.ImageField(max_length=500, upload_to='usu/', blank=True, null=True)
    genero = models.ForeignKey(Genero, db_column='genero')
    fecha_de_nacimineto = models.DateField()
    fecha_de_registro = models.DateField(auto_now_add=True)
    direccion = models.CharField(max_length=500)
    credito_disponible = models.DecimalField(max_digits=12, decimal_places=2)
    ganancia_obtenida = models.DecimalField(max_digits=12, decimal_places=2)
    clase_cliente = models.CharField(max_length=1)
    rol = models.ForeignKey(Rol)

    def __unicode__(self):
        return unicode(self.nombre) or u''

    class Meta:
        managed = False
        db_table = 'usuario'