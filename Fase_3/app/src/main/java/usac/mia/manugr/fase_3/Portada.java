package usac.mia.manugr.fase_3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.telecom.Connection;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Statement;

public class Portada extends Activity {

    Statement st;
    ImageView logo;
    VideoView vid;
    TextView mision, vision, about, eslogan, nombre;
    Button login, productos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portada);

        nombre = (TextView) findViewById(R.id.txt_nombre_Portada);
        logo = (ImageView) findViewById(R.id.txt_logo);
        vid = (VideoView) findViewById(R.id.Video_Portada);
        mision = (TextView) findViewById(R.id.txt_Mision);
        vision = (TextView) findViewById(R.id.txt_Vision);
        about = (TextView) findViewById(R.id.txt_About);
        eslogan = (TextView) findViewById(R.id.txt_Eslogan);

        login = (Button) findViewById(R.id.btn_Login_Portada);
        login.setOnClickListener(new OnClickListener(){
            public void onClick(View arg0)
            {   Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);

            }   });
        productos = (Button) findViewById(R.id.btn_Productos_Portada);
        productos.setOnClickListener(new OnClickListener(){
            public void onClick(View arg0)
            {   Intent i = new Intent(getApplicationContext(), Catalogo.class);
                startActivity(i);

            }   });


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {
            ConnectOra conn = new ConnectOra("10.0.2.2", "XE", "Fase_3", "1234");
            st = conn.getConexion().createStatement();
            ResultSet resultado = st.executeQuery("SELECT * From PORTADA WHERE ID = 1");

            if(resultado.next())
            {
                nombre.setText(resultado.getString("NOMBRE"));
                mision.setText(resultado.getString("MISION"));
                vision.setText(resultado.getString("VISION"));
                eslogan.setText(resultado.getString("ESLOGAN"));
                about.setText(resultado.getString("ABOUT_ME"));

                String path="http://10.0.2.2:8000/media/"+resultado.getString("VIDEO");

                Uri uri=Uri.parse(path);

                vid.setVideoURI(uri);
                vid.start();

                path = "http://10.0.2.2:8000/media/"+resultado.getString("LOGO");
                URL url = new URL(path);
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                logo.setImageBitmap(bmp);
            }

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
