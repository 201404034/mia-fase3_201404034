from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import (
	index_view,
	reportes_view,
	UsuarioList,
	UsuarioDetail,
	UsuarioCreation,
	UsuarioUpdate,
	UsuarioDelete
)

urlpatterns = [
	url(r'^(?P<pk>[0-9+])$', index_view, name='home'),
#############
#  Usuario  #
#############
	url(r'^Usuarios/$', UsuarioList.as_view(), name='list'),
    url(r'^Usuario(?P<pk>\d+)$', UsuarioDetail.as_view(), name='detail'),
    url(r'^Usuario/nuevo$', UsuarioCreation.as_view(), name='new'),
    url(r'^Usuario/editar/(?P<pk>\d+)$', UsuarioUpdate.as_view(), name='edit'),
    url(r'^Usuario/borrar/(?P<pk>\d+)$', UsuarioDelete.as_view(), name='delete'),
    url(r'^Reportes/(?P<pk>\d+)$', reportes_view, name='reportes')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)