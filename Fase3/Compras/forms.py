from django.forms import ModelForm
from django import forms
from .models import Compra
    

class Compra_Form(ModelForm):
    class Meta:
    	model = Compra
    	fields = ['cantidad']