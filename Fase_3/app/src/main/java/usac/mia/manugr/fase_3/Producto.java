package usac.mia.manugr.fase_3;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.Statement;

public class Producto extends Activity {

    TextView nombre, precio, descripcion, fecha, categoria, disponibles;
    ImageView imagen;
    Button add;
    Statement st;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        nombre = (TextView) findViewById(R.id.txt_Nombre_Producto);
        precio = (TextView) findViewById(R.id.txt_Precio_Producto);
        descripcion = (TextView) findViewById(R.id.txt_Descripcion_Producto);
        fecha = (TextView) findViewById(R.id.txt_Fecha_Producto);
        categoria = (TextView) findViewById(R.id.txt_Categoria_Producto);
        disponibles = (TextView) findViewById(R.id.txt_Disponibles_Producto);
        imagen = (ImageView) findViewById(R.id.Foto_Producto);
        add = (Button) findViewById(R.id.btn_AddCarrito_Producto);

        try{
            ConnectOra conn = new ConnectOra("10.0.2.2", "XE", "Fase_3","1234");
            Statement st = conn.getConexion().createStatement();
            Bundle bundle = getIntent().getExtras();
            id = bundle.getString("ID");
            ResultSet resultado = st.executeQuery("SELECT * FROM Producto WHERE ID = "+id);
            if(resultado.next()){
                nombre.setText(resultado.getString("NOMBRE"));
                precio.setText(resultado.getString("PRECIO"));
                descripcion.setText(resultado.getString("DESCRIPCION"));
                disponibles.setText(resultado.getString("Disponibles"));

                String path = "http://10.0.2.2:8000/media/"+resultado.getString("IMAGEN");
                URL url = new URL(path);
                Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                imagen.setImageBitmap(bmp);

                resultado = st.executeQuery("SELECT * FROM Categoria WHERE ID = "+resultado.getString("CATEGORIA_ID"));

                if(resultado.next()){
                    categoria.setText(resultado.getString("NOMBRE"));
                }
            }
            resultado.close();
            st.close();
        }catch(Exception e) {
        }


    }
}
