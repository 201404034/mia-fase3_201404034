from django.shortcuts import render, get_object_or_404
from .models import Usuario, Producto, Categoria 

# Create your views here.
def Bonito_View(request):
	productos = Producto.objects.all()
	categorias = Categoria.objects.all()
	subcategorias = Categoria.objects.all()
	usu = request.user

	busqueda = request.GET.get("search")
	categoria = request.GET.get("categoria")
	subcategoria = request.GET.get("subcategoria")

	if busqueda:
		productos = productos.filter(nombre__icontains = busqueda)
	if categoria and (categoria != '!'):
		subcategorias = subcategorias.filter(Padre=categoria)
	if subcategoria:
		productos = productos.filter(categoria = subcategoria or categoria)
		subcategorias = Categoria.objects.exclude(Padre__isnull=True)
	else:
		subcategorias = Categoria.objects.exclude(Padre__isnull=True)
	context = {
		"productos": productos,
		"categorias": categorias,
		"subcategorias": subcategorias,
	}
	return render(request, "caquero.html", context)