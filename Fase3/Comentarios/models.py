from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
# Create your models here.
class Carrito(models.Model):
    id = models.BigIntegerField(primary_key=True)
    usuario = models.ForeignKey('Usuario')

    def __unicode__(self):
        return unicode(self.id) or u''

    class Meta:
        managed = False
        db_table = 'carrito'

class Compra(models.Model):
    id = models.BigIntegerField(primary_key=True)
    cantidad = models.BigIntegerField()
    carrito = models.ForeignKey(Carrito)
    producto = models.ForeignKey('Producto', related_name='compra_producto')

    def __unicode__(self):
        return unicode(self.producto.nombre) or u''

    class Meta:
        managed = False
        db_table = 'compra'

class Categoria(models.Model):
    id = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=1500)
    categoria = models.ForeignKey('self', blank=True, null=True, related_name='Padre')

    def __unicode__(self):
        return unicode(self.nombre) or u''

    class Meta:
        managed = False
        db_table = 'categoria'

class Comentario(models.Model):
    id = models.BigIntegerField(primary_key=True)
    titulo = models.CharField(max_length=25)
    fecha = models.DateField(auto_now_add=True)
    ponderacion = models.BigIntegerField()
    cuerpo = models.CharField(max_length=1500)
    compra = models.ForeignKey('Compra')
    comentario = models.ForeignKey('self', blank=True, null=True, related_name='respuesta')

    def __unicode__(self):
        return unicode(self.titulo) or u''

    class Meta:
        managed = False
        db_table = 'comentario'

class Producto(models.Model):
    id = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=50)
    imagen = models.ImageField(max_length=500, upload_to='prod/')
    descripcion = models.CharField(max_length=1500)
    precio = models.DecimalField(max_digits=10, decimal_places=2)
    fecha_de_publicacion = models.DateField(auto_now_add=True)
    disponibles = models.BigIntegerField()
    categoria = models.ForeignKey(Categoria)
    usuario = models.ForeignKey('Usuario', related_name='provedor')

    def __unicode__(self):
        return unicode(self.nombre) or u''

    class Meta:
        managed = False
        db_table = 'producto'

class Rol(models.Model):
    id = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=25)

    def __unicode__(self):
        return unicode(self.nombre) or u''

    class Meta:
        managed = False
        db_table = 'rol'


class Genero(models.Model):
    genero = models.CharField(primary_key=True, max_length=10)

    def __unicode__(self):
        return unicode(self.genero) or u''
        
    class Meta:
        managed = False
        db_table = 'genero'


class Usuario(models.Model):
    magia = models.OneToOneField(User, related_name='Usu_coment')
    id = models.BigIntegerField(primary_key=True)
    nombre = models.CharField(max_length=25)
    apellido = models.CharField(max_length=25)
    correo = models.EmailField(max_length=50)
    telefono = models.CharField(max_length=12, blank=True, null=True)
    foto = models.ImageField(max_length=500, upload_to='usu/', blank=True, null=True)
    genero = models.ForeignKey(Genero, db_column='genero')
    fecha_de_nacimineto = models.DateField()
    fecha_de_registro = models.DateField(auto_now_add=True)
    direccion = models.CharField(max_length=500)
    credito_disponible = models.DecimalField(max_digits=12, decimal_places=2)
    ganancia_obtenida = models.DecimalField(max_digits=12, decimal_places=2)
    clase_cliente = models.CharField(max_length=1)
    rol = models.ForeignKey(Rol)

    def __unicode__(self):
        return unicode(self.nombre) or u''

    class Meta:
        managed = False
        db_table = 'usuario'
